/**
 * @module index.js
 * @summary Provides the logic for Foo Café Beacon Hunt
 * @author Anders Borg <anders.borg@abiro.com>
 * @version 1.0
 */

/* global device */
/* global assocs */

// Debug flag
var DEBUG = true;

// Beacon information
var BEACONREGION = "com.abiro.beaconhunt";
var BEACONUUID = "A6E3E006-BBC4-4DD1-9CA3-3E86C4957DF0";

// Beacon proximities
var PROXIMMEDIATE = 1;
var PROXNEAR = 2;
var PROXFAR = 3;
var PROXUNKNOWN = 4;

// Threshold proximity
var PROXACCEPTABLE = PROXNEAR;

// Widgets
var appbanner = null;
var regionstatus = null;
var beaconstatus = null;

// Beacon globals
var cplm = null;
var delg = null;
var region = null;

// App logic globals
var platform = '';
var beaconfound = 0;
var beaconrendered = -1;
var beaconmonitored = 0;

function onDeviceReady() {
    platform = device.platform.toLowerCase();

    bindEvents();

    initViews();

    initBeacons();

    regionstatus.innerHTML = express("Welcome to Foo Café Beacon Hunt!") + express("We're ready to go!");

    reset();
}

function bindEvents() {
    document.addEventListener("pause", function() {
        // Save etc
    }, false);

    document.addEventListener("resume", function() {
        // Restore etc
    }, false);
}

function initViews() {
    appbanner = document.getElementById("appbanner");
    regionstatus = document.getElementById("regionstatus");
    beaconstatus = document.getElementById("beaconstatus");
    debug = document.getElementById("debug");
}

function initBeacons() {
    try {
        cplm = cordova.plugins.locationManager;

        // Enable Bluetooth
        if (platform === "android") {
            cordova.plugins.locationManager.enableBluetooth();
        }

        // Event delegate
        delg = new cplm.Delegate();
        cplm.setDelegate(delg);

        // Events
        delg.didFinishLaunchingWithOptions = didFinishLaunchingWithOptions;
        delg.didEnterRegion = didEnterRegion;
        delg.didExitRegion = didExitRegion;
        delg.didDetermineStateForRegion = didDetermineStateForRegion;
        delg.didStartMonitoringForRegion = didStartMonitoringForRegion;
        delg.didRangeBeaconsInRegion = didRangeBeaconsInRegion;

        // Initialize region
        region = new cplm.BeaconRegion(BEACONREGION, formatUUID(BEACONUUID), undefined, undefined, true);

        // Allow beacon detection always (redundant for this app)
        if (platform === "ios") {
            cplm.requestAlwaysAuthorization();
        }

        // Start monitoring for region
        cplm.startMonitoringForRegion(region).fail(console.error).done();
    }
    catch (e) {
        alert("Error: initBeacons: " + e.message);
    }
}

function didFinishLaunchingWithOptions(result) {
    // Possible behavior based on how the app was started
}

function didEnterRegion(result) {
    // What we need is handled by didDetermineStateForRegion
}

function didExitRegion(result) {
    // What we need is handled by didDetermineStateForRegion
}

function didDetermineStateForRegion(result) {
    try {
        switch (result.state) {
            case "CLRegionStateInside":
                region.state = "inside";
                regionstatus.innerHTML = express("Inside beacon region");
                cplm.startRangingBeaconsInRegion(region).fail(console.error).done();
                break;

            case "CLRegionStateOutside":
                region.state = "outside";
                regionstatus.innerHTML = express("Outside beacon region");
                cplm.stopRangingBeaconsInRegion(region).fail(console.error).done();
                break;

            default:
                break;
        }
    }
    catch (e) {
    }
}

function didStartMonitoringForRegion(result) {
    // Redundant for this app
}

function didRangeBeaconsInRegion(result) {
    var beacons = result.beacons;
    if (beacons instanceof Array) {
        var assoc;

        // Have we found a new one?

        if (beaconfound < assocs.length) {
            assoc = assocs[beaconfound];

            for (var b in beacons) {
                var beacon = beacons[b];

                if (prox2int(beacon.proximity) <= PROXACCEPTABLE && parseInt(beacon.major) === assoc.major && parseInt(beacon.minor) === assoc.minor) {
                    beaconfound++;
                    assoc = assocs[beaconfound];
                    break;
                }
            }
        }

        // Beacon status

        if (beaconrendered < beaconfound) {
            render = "";

            // More beacons to find
            if (beaconfound < assocs.length) {
                // The current beacon
                if (beaconfound > 0) {
                    var assoc = assocs[beaconfound - 1];

                    render += express("You found " + assocInfo(beaconfound, assoc) + "!");
                    render += express("Here's something entertaining");
                    render += assoc.content;
                    render += getRule();
                }

                // Clue for the next beacon
                assoc = assocs[beaconfound];
                render += express("Clue");
                render += express("You will find " + assocInfo(beaconfound + 1, assoc) + ".");
            }

            // All beacons have been found
            else if (beaconfound === assocs.length) {
                render += express("You found all the beacons !!!!!");
                render += express("You'll get pizza, beer and respect!");
                render += getImage("images/success.png");
            }

            beaconrendered = beaconfound;
            beaconstatus.innerHTML = render;
        }

        // Debugging

        if (DEBUG) {
            render = "";

            render += getRule();
            render += getHeader("DEBUG");

            if (beaconfound < assocs.length) {
                render += getHeader("Beacon to look for", 3);
                render += getParagraph(JSON.stringify(assocs[beaconfound]));
            }

            render += getHeader("Detected beacons (" + (++beaconmonitored) + ")", 3);

            for (var b in beacons) {
                render += getParagraph(JSON.stringify(beacons[b]));
            }

            debug.innerHTML = render;
        }
    }
}

function reset() {
    beaconfound = 0;
    beaconrendered = -1;

    beaconstatus.innerHTML = "";
    debug.innerHTML = "";
}

function prox2int(proximity) {
    switch (proximity.toLowerCase()) {
        case "proximityimmediate":
        case "immediate":
            return PROXIMMEDIATE;

        case "proximitynear":
        case "near":
            return PROXNEAR;

        case "proximityfar":
        case "far":
            return PROXFAR;

        default:
            return PROXUNKNOWN;
    }
}

function formatUUID(value) {
    return value.toLowerCase();
}

function assocInfo(number, assoc) {
    return "beacon #" + number + " (the " + assoc.name + " one with major ID " + assoc.major + " and minor ID " + assoc.minor + ") at " + assoc.clue;
}

function express(text) {
    ttshelper(text);

    return getHeader(text);
}