/**
 * @module assocs.js
 * @summary Provides the beacon and content associations for Foo Café Beacon Hunt
 * @author Anders Borg <anders.borg@abiro.com>
 * @version 1.0
 */

var assocs = [
    {
        major: 22240,
        minor: 101,
        name: "big grey",
        clue: "the Cirkus Imago poster",
        content: "<img src='images/image1.gif'/>"
    },
    {
        major: 22240,
        minor: 11,
        name: "blue",
        clue: "the copier",
        content: "<img src='images/image2.gif'/>"
    },
    {
        major: 22240,
        minor: 12,
        name: "green",
        clue: "the coffee machine",
        content: "<iframe width=300 height=200 src='https://www.youtube.com/embed/wj1dUA0QKes?feature=player_embedded' frameborder=0 allowfullscreen></iframe>"
    },
    {
        major: 22240,
        minor: 13,
        name: "yellow",
        clue: "by the entrance",
        content: "<img src='images/image4.gif'/>"
    },
    {
        major: 22240,
        minor: 14,
        name: "red",
        clue: "the speaker stand",
        content: "<img src='images/image5.gif'/>"
    }
];
