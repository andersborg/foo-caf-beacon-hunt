// https://www.npmjs.com/package/cordova-plugin-tts

var talking = false;
var texts = [];

var localeDefault = "en-US";
var rateDefault = 1;

function ttshelper(text, locale, rate) {
    locale = locale || localeDefault;
    rate = rate || rateDefault;

    if (talking) {
        var entry = {
            text: text,
            locale: locale,
            rate: rate
        };
        texts.push(entry);
    }
    else {
        talking = true;

        TTS.speak(
                {
                    text: text,
                    locale: locale,
                    rate: rate
                },
                function() {
                    console.log("texts: " + JSON.stringify(texts));
                    talking = false;
                    if (texts.length > 0) {
                        var entry = texts[0];
                        texts.splice(0, 1);
                        ttshelper(entry.text, entry.locale, entry.rate);
                    }
                },
                function(reason) {
                    console.log(reason);
                }
        );
    }
}
