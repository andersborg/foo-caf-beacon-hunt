/**
 * @module html.js
 * @summary Provides basic abstraction of HTML for Foo Café Beacon Hunt
 * @author Anders Borg <anders.borg@abiro.com>
 * @version 1.0
 */

/**
 * getParagraph
 * @param {String} text Plain text
 * @returns {String}
 */
function getParagraph(text) {
    return "<p>" + html(text) + "</p>";
}

/**
 * getHeader
 * @param {String} text Plain text
 * @param {Number} level Header level (1 to 6)
 * @returns {String}
 */
function getHeader(text, level) {
    level = level || 2;

    return "<h" + level + ">" + html(text) + "</h" + level + ">";
}

/**
 * getImage
 * @param {String} path File path to the image
 * @returns {String}
 */
function getImage(path) {
    return "<img src='" + html(path) + "' alt='image' />";
}

/**
 * getRule
 * @returns {String}
 */
function getRule() {
    return "<hr/>";
}

/**
 * html
 * @param {String} text Plain text
 * @returns {String}
 */
function html(text) {
    return text
            .replace(/&/g, "&amp;")
            .replace(/>/g, "&gt;")
            .replace(/</g, "&lt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&apos;")
            ;
}
