# README #

* Foo Café Beacon Hunt
* 1.0

This project is as-is intended for PhoneGap Build.

It's easy to convert it into a PhoneGap/Cordova command line project:

Store config.xml above www.

Edit config.xml:
Change any res/ references to www/res/.
Put www/ in front of icon.png and splash.png.

Add the following plugins (cordova plugin add):

* cordova-plugin-whitelist
* cordova-plugin-device
* cordova-plugin-inappbrowser
* cordova-plugin-dialogs
* cordova-plugin-globalization
* cordova-plugin-ibeacon
* cordova-plugin-tts
* https://github.com/robertklein/cordova-ios-security

Build with cordova build android/ios.

Run with cordova run android/ios.